#!/usr/bin/env bash
# -------------------------------------------------------------------------
# Script : screc.sh
# Descrição : grava sua tela em formato de MP4.
# Versão : 1.3
# Autor : Eppur Si Muove
# Contato : eppur.si.muove@keemail.me
# Criação : 20/01/2020
# Modificação : 29/10/2024
# Licença : GNU/GPL v3.0
# -------------------------------------------------------------------------

help='
# -----------------------------------------------------------------
# Exemplo de uso:
#  $ ./screc.sh [-s | --slop]
#  $ Pressione CTRL+C para parar a gravação
#
#  -s | --slop
#  Parâmetro opcional. Permite selecionar uma área específica da
#  tela a ser gravada. Esse recurso depende da ferramenta slop.
#  Para instalar o slop: # apt install slop
# -----------------------------------------------------------------
'

# Define nome do arquivo de vídeo
nvideo=$(date +'%Y%m%d-%H%M%S')
nvideo=$(xdg-user-dir VIDEOS)/$nvideo.mp4

# Define área da tela
case $1 in
	"-s"|"--slop" )
		[[ -z $(which slop) ]] && echo "Instale a slop com: # apt instal slop" && exit 1
		echo "Selecione a área da tela a ser gravada..."
		geometria=$(slop -k -f "%w %h %x %y")
		width=$(cut -d' ' -f1 <<< $geometria)
		height=$(cut -d' ' -f2 <<< $geometria)
		xOffset=$(cut -d' ' -f3 <<< $geometria)
		yOffset=$(cut -d' ' -f4 <<< $geometria)
		i=":0.0+$xOffset,$yOffset"
		resolucao="${width}x${height}"
		;;
	"-h"|"--help" )
		echo "$help"
		exit 0
		;;
	* )
		i=":0.0"
		resolucao=$(xrandr | grep '*' | cut -d ' ' -f4)
		;;
esac

# Início do programa
echo "Pressione CTRL+C para parar a gravação"
ffmpeg -f x11grab -y -s $resolucao -i $i -c:v libx264 -crf 21 -preset faster -pix_fmt yuv420p -maxrate 5000K -bufsize 5000K -vf 'scale=if(gte(iw\,ih)\,min(1920\,iw)\,-2):if(lt(iw\,ih)\,min(1920\,ih)\,-2)' -movflags +faststart -c:a aac -b:a 160k $nvideo &> /dev/null

# Gravando até que o usuário tecle CTRL+C
