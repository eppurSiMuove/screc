# screc

Grava sua tela em formato MP4 usando ffmpeg.

# Exemplo de uso:
    $ ./screc.sh [-s | --slop]
    $ Pressione CTRL+C para parar a gravação
	
    -s | --slop
		Parâmetro opcional. Permite selecionar uma área específica da
		tela a ser gravada. Esse recurso depende da ferramenta slop.
		Para instalar o slop: # apt install slop
